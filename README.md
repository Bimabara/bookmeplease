# BookMePlease
Tugas Kelompok E01 - Mata Kuliah Pemrograman Berbasis Platform
Fakultas Ilmu Komputer UI, Semester Ganjil 2021/2022

# Anggota Kelompok 
1. Setasena Randata Ramadanie 2006482792
2. Marthin Daniel Theo Gratia  1906306786
3. Bimabara Sukma Muryanto 1906318893
4. Muhammad Faras Baginda 1906398673
5. Alfina Azaria 2006482773
6. Bima Sudarsono Adinsa 1906399083

# Heroku Link
Coming Soon 

# Modul
1. Register Form + Login User
2. Form genre book interest
3. Landing Page
4. Profile User + Wishlist
5. Form Rekomendasi Buku
6. Admin Page

# Database: 
- Rekomendasi Buku
- Wishlist user
- Profil

# Deskripsi BookMePlease
Pandemi covid-19 yang menggemparkan dunia tentunya memaksa seluruh manusia untuk beralih menuju era digitalisasi. Hal tersebut sekaligus mengimplikasikan bahwa seluruh aktivitas tidak dapat dilakukan secara offline, melainkan full online. Tentunya, sebagai makhluk sosial, menghabiskan waktu di rumah kadang menjadi tantangan. Kemalasan hingga rasa bosan dengan cepat menghantui diri kita. Oleh karena itu, kelompok kami akan membuat sebuah solusi mengatasi rasa bosan semasa pandemi dengan membuat sebuah website bernama BookMePlease. BookMePlease adalah sebuah website yang memberikan rekomendasi buku yang menarik, sesuai dengan preferensi masing-masing pengguna. BookMePlease  menyajikan berbagai macam genre, sehingga memungkinkan para user untuk melakukan eksplorasi buku yang ingin dibaca.


# Persona
1. Admin
Admin bertugas untuk mengurus otentikasi dan otorisasi user, admin juga melakukan verifikasi rekomendasi buku yang diberikan oleh user yang memiliki akun, ketika rekomendasi buku telah diverifikasi oleh admin maka buku rekomendasi akan muncul pada landing page.

2. User yang tidak memiliki akun
User yang tidak memiliki akun hanya dapat melihat rekomendasi buku berdasarkan genre pada landing page.

3. User yang memiliki akun
User yang memiliki akun dapat mengisi form rekomendasi buku, mendapat rekomendasi buku berdasarkan pengisian form,  dan membuat wishlist bukunya sendiri.
