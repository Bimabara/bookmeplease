from django.apps import AppConfig


class TkFolderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tk_folder'
